package com.practice.todoapp.data;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Task, Long>{

	Iterable<Task> findAllByDescription(String description);
}
