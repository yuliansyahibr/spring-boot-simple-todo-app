package com.practice.todoapp.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.practice.todoapp.TodoappApplication;
import com.practice.todoapp.data.Task;
import com.practice.todoapp.data.TodoRepository;

@RestController
@CrossOrigin
public class TodoController {
	
	@Autowired
	private TodoRepository todoRepository;
	
	@GetMapping(produces = "application/json")
	public Iterable<Task> getAll() {
		return todoRepository.findAll();
	}
	
	@GetMapping(value = "/task/{id}", produces = "application/json")
	public Task getTask(@PathVariable("id") Long id) {
		return todoRepository.findById(id).get();
	}
	
	@PostMapping(value = "/task/create")
	public Task createTask(@RequestBody Task newTask) {
		try {
			newTask.setIsCompleted(false);
			return todoRepository.save(newTask);
		} catch (Exception e) {
			return null;
		}
	}
	
	@PutMapping(value = "/task/update/{id}", produces = "application/json")
	public Task updateTask(@RequestBody Task updatedTask, @PathVariable Long id) {
		Logger log = LoggerFactory.getLogger(TodoappApplication.class);
		log.info("isComplete="+updatedTask.toString());
		return todoRepository.findById(id).map(task -> {
			task.setTitle(updatedTask.getTitle());
			task.setDescription(updatedTask.getDescription());
			task.setDueDate(updatedTask.getDueDate());
			task.setIsCompleted(updatedTask.getIsCompleted());
			return todoRepository.save(task);
		}).orElseGet(() -> {
			updatedTask.setId(id);
			return todoRepository.save(updatedTask);
		});
	}
	
	@PutMapping(value = "/task/complete/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
	public Task updateComplete(@RequestBody String completed, @PathVariable Long id) {
		Logger log = LoggerFactory.getLogger(TodoappApplication.class);
		log.info("isComplete="+completed.toString());
		return todoRepository.findById(id).map(task -> {
			Boolean isCompleted = Boolean.valueOf(completed);
			task.setIsCompleted(isCompleted);
			return todoRepository.save(task);
		}).orElseGet(() -> {
			return null;
		});
	}
	
	@DeleteMapping("/task/delete/{id}")
	public Boolean deleteTask(@PathVariable Long id) {
		try {
			todoRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
