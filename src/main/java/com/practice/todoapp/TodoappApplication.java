package com.practice.todoapp;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.practice.todoapp.data.Task;
import com.practice.todoapp.data.TodoRepository;

@SpringBootApplication
public class TodoappApplication {

	private static final Logger log = LoggerFactory.getLogger(TodoappApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(TodoappApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner fillData(TodoRepository todoRepository) {
		return (args) -> {
			todoRepository.save( new Task(
					"Belajar AAD",
					"Belajar dasar-dasar materi di kelas Dicoding",
					1577811600000L,
					false
			));
			todoRepository.save(new Task(
				    "Simulasi Ujian AAD",
				    "Mencoba mengerjakan project latihan AAD",
				    1655226000000L,
				    true
			));
			todoRepository.save(new Task(
					"Ambil Ujian AAD",
					"Saatnya menjadi Associate Android Developer!",
					1735578000000L,
					false
			));
			
			// fetch all customers
	      log.info("Tasks found with findAll():");
	      log.info("-------------------------------");
	      for (Task todo: todoRepository.findAll()) {
	        log.info(todo.toString());
	      }
	      log.info("");
//		      
//		   // fetch an individual customer by ID
//	      Optional<Task> optional = todoRepository.findById(1L);
//	      optional.map(_task -> {
//	    	  log.info("Task found with findById(1L):");
//		      log.info("--------------------------------");
//		      log.info(_task.toString());
//		      log.info("");
//		      return _task;
//	      });
//		      
//		   // fetch customers by last name
//	      log.info("Task found with findAllByDescription('belajar'):");
//	      log.info("--------------------------------------------");
//	      todoRepository.findAllByDescription("belajar").forEach(_todo -> {
//	        log.info(_todo.toString());
//	      });
//	      // for (Customer bauer : repository.findByLastName("Bauer")) {
//	      //  log.info(bauer.toString());
//	      // }
//	      log.info("");
		};
	}
	
}
