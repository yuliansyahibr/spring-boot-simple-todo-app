package com.practice.todoapp.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Task {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String title;
	private String description;
	@Column(name = "due_date")
	private Long dueDate;
	@Column(name = "completed")
	private Boolean isCompleted;
	
	@Override
	public String toString() {
		return String.format("%s, %s, %d, %s", title, description, dueDate, isCompleted.toString());
	}
	
	protected Task() {
		super();
	}
	public Task(String title, String description, Long dueDate, Boolean isCompleted) {
		super();
		this.description = description;
		this.dueDate = dueDate;
		this.isCompleted = isCompleted;
		this.title = title;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDueDate() {
		return dueDate;
	}
	public void setDueDate(Long dueDate) {
		this.dueDate = dueDate;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
	
}
